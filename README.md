# Predicting gene essentiality in *Caenorhabditis elegans* by feature engineering and machine-learning #


* To download this repository:

```
git clone https://www.bitbucket.org/tuliocampos/essential_elegans.git
```
- Requirements: R (systematic feature selection and machine-learning)

- Optional: R-Markdown, BASH and Python (feature engineering)

## What is the content? ##

* R and shell scripts used to download publicly available data and to extract features: "Rmarkdown\_Features\_Plots.Rmd" (not fully automated)
* The file containing the features extracted for each gene (features with low variance were removed): "Feature\_collection.txt"
* Coordinate files for the assessment of genomic locations: "\*.coord" files 
* Nucleotide and protein sequences per gene (provisional annotations)): "\*.fasta" files
* Gene lists per (provisional) essentiality annotation: "\*list.txt" files - for the final ML predictions see the SupplementaryTable (S10)
* Scripts used for the systematic feature selection and machine-learning approaches (using the FULL, NR and NR\_SELECTED data sets - includes the 1000bootstraps evaluation): see the ".R" files
* The number of SNPs identified by genomic location using the VCF file obtained from https://www.elegansvariation.org: "Frequencies_SNPs.txt"  
* Software and library versions: SoftwareVersions.txt and sessionInfo.txt
* The final set of 28 highly predictive features of gene essentiality: Surviving\_variables.txt
* Script used to calculate Ka/Ks - obtained from: https://github.com/MerrimanLab/selectionTools/blob/master/extrascripts/kaks.py 
* Ordered essentiality scores for plotting: Essentiality\_scores.txt 

## How to explore this repository? ##

* Use the R-Markdown file "Rmarkdown\_Features\_Plots.Rmd" to explore how the features were obtained and engineered from publicly available data, and also see how the plots were generated. 
* Load the feature collection into an R data.frame (columns represent features, rows represent genes): 

```
x=read.delim("Feature_collection.txt",sep="\t",header=T,row.names=1)

#The first feature names
head(colnames(x))
#The first gene names
head(rownames(x))
``` 

* Filters can be applied to select features and/or genes. For example, selecting only the 28 most predictive feaures identified in the study:

```
library(dplyr)
best_pred=read.delim("28_best_predictors.txt",header=F)
x=x %>% dplyr::select(one_of(as.character(best_pred$V1)))
```

* To select "essential" or "non-essential" gene features, load and use the "\*list.txt" files and use as filters.

For example, to select essential gene (provisional) features:

```
essential=read.delim("Essential_NR_list.txt",header=F)
x=x[which(rownames(x) %in% as.character(essential$V1)),]
```
----

* To convert the data.frame into a matrix format (if necessary, for ML inputs):

```
x_names=rownames(x)
x=sapply(x,as.numeric)
rownames(x)=x_names
```

* Because the data sets are readily available and the R scripts for the ML approaches use them directly, you can skip the previous data preparation steps and just run the systematic analyses using any of the data sets (FULL, NR or NR\_SELECTED)

For example, you can run the systematic approach using the NR\_SELECTED data set using a shell command:

```
Rscript NR_SELECTED_script.R 
```

You can also run the scripts using Rstudio! 

Modify the R scripts if you want to use more or less than 32 CPU cores


## Licence ##

MIT

## Cite ##

If you use these scripts or data, please cite:

**Predicting gene essentiality in Caenorhabditis elegans by feature engineering and machine-learning**

Tulio L Campos, Pasi K Korhonen, Paul W Sternberg, Robin B Gasser, Neil D Young

**Computational and Structural Biotechnology Journal**, Volume 18, July 2020, 1093-1102, https://doi.org/10.1016/j.csbj.2020.05.008
