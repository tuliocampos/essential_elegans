library(ggthemes)

##

x=read.delim("Feature_collection.txt",header=T,sep="\t",row.names=1)

x_names=rownames(x)

x=sapply(x,as.numeric)

rownames(x)=x_names

dim(x)

class(x)
typeof(x)

x1=x[1:670,]
x2=x[671:16740,]


####Data matrix (non-redundant)

#Essential_FULL_list=read.table("Essential_NR_list.txt")
#NEssential_FULL_list=read.table("NEssential_NR_list.txt")

#x1=x1[which(Essential_FULL_list$V1 %in% rownames(x1)),]
#x2=x2[which(NEssential_FULL_list$V1 %in% rownames(x2)),]

#dim(x1)
#dim(x2)

#########Best predictors only###########

#surviv=read.table("Surviving_variables.txt")

#x1=x1[,as.character(surviv$V1)]
#x2=x2[,as.character(surviv$V1)]

##


x=rbind(x1,x2)

labels = c(rep("E", dim(x1)[1]), rep("N", dim(x2)[1]))
labels_ = c(rep(1, dim(x1)[1]), rep(0, dim(x2)[1]))

prefix = "CE"


###Machine-learning steps

library("caret")

#Removing near zero variance features

nzv <- nearZeroVar(x)
x <- x[,-nzv]

x1 <- x1[,-nzv] 
x2 <- x2[,-nzv]


#Create vectors to store performance results 

test_rf <- vector(mode="numeric", length=9)
test_svm <- vector(mode="numeric", length=9)
test_gbm <- vector(mode="numeric", length=9)
test_xgb <- vector(mode="numeric", length=9)
test_glm <- vector(mode="numeric", length=9)
test_nn <- vector(mode="numeric", length=9)
test_default <- vector(mode="numeric", length=9)

train_rf <- vector(mode="numeric", length=9)
train_svm <- vector(mode="numeric", length=9)
train_gbm <- vector(mode="numeric", length=9)
train_xgb <- vector(mode="numeric", length=9)
train_glm <- vector(mode="numeric", length=9)
train_nn <- vector(mode="numeric", length=9)
train_default <- vector(mode="numeric", length=9)

pr_rf <- vector(mode="numeric", length=9)
pr_svm <- vector(mode="numeric", length=9)
pr_gbm <- vector(mode="numeric", length=9)
pr_xgb <- vector(mode="numeric", length=9)
pr_glm <- vector(mode="numeric", length=9)
pr_nn <- vector(mode="numeric", length=9)
pr_default <- vector(mode="numeric", length=9)

pr_train_rf <- vector(mode="numeric", length=9)
pr_train_svm <- vector(mode="numeric", length=9)
pr_train_gbm <- vector(mode="numeric", length=9)
pr_train_xgb <- vector(mode="numeric", length=9)
pr_train_glm <- vector(mode="numeric", length=9)
pr_train_nn <- vector(mode="numeric", length=9)
pr_train_default <- vector(mode="numeric", length=9)


library("doMC")
library("pROC")
library("PRROC")
library("caret")


rfGrid <- expand.grid(.mtry=c(1,5,10,20,50,100))

gbmGrid <-  expand.grid(interaction.depth = c(1,3,5,10,20), 
                        n.trees = (1:10)*200,
                        shrinkage = 0.01,
                        n.minobsinnode = 10)

svmGrid <- expand.grid(sigma = c(.01,.02,.05,0.1),
                       C = c(0.1,0.75, 0.9))

lmGrid <-  expand.grid(alpha = 1,lambda = seq(0.001,0.1,by = 0.001))

nnGrid <-  expand.grid(size = seq(from = 5, to = 20, by = 5))


xgbGrid <- expand.grid(nrounds = c(100,200,500),
                       max_depth = c(3,5,10),
                       colsample_bytree = 1,
                       eta = 0.1,
                       gamma=1,
                       min_child_weight = 1,
                       subsample = 1)


fitControl <- trainControl(method = "cv",
                           number = 5,
                           classProbs = TRUE,
                           summaryFunction = twoClassSummary)

registerDoMC(cores = 32)

for(i in seq(10, 90, by = 10)) {

set.seed(111)

  print(i/10)

  tr.idx = c(sample(1:nrow(x1), round(dim(x1)[1]/100*i)),
             sample(nrow(x1)+1:nrow(x2), round(dim(x2)[1]/100*i)))
  te.idx = setdiff(1:nrow(x), tr.idx)
  x.tr = x[tr.idx, ]
  x.te = x[te.idx, ]
  y.tr = labels[tr.idx]
  y.te = labels[te.idx]

  y.te_num = labels_[te.idx]
  y.tr_num = labels_[tr.idx]
 
  #Feature selection - internal

  library("glmnet")

  fit.lasso <- glmnet(x.tr, y.tr_num, family="binomial", alpha=1)
  fit.ridge <- glmnet(x.tr, y.tr_num, family="binomial", alpha=0)
  fit.elnet <- glmnet(x.tr, y.tr_num, family="binomial", alpha=.5)

  for (k in seq(0,10,by=5)) {
    assign(paste("fit", k, sep=""), cv.glmnet(x.tr, y.tr_num,
    type.measure="auc",alpha=k/10,family="binomial",parallel = TRUE))
  }

  elastic1=as.matrix(coef(fit5,fit5$lambda.min))

  inds<-which(elastic1!=0)
  VARSELECT1_temp=row.names(elastic1)[inds]
  VARSELECT1=VARSELECT1_temp[!(VARSELECT1_temp %in% '(Intercept)')];

  library("enpls")

  VARSELECT2_1=enspls.fs(x.tr, y.tr_num, maxcomp = 10L, cvfolds = 5L, alpha = seq(0.2, 0.8, 0.2),
                         reptimes = 10L, method = c("mc", "boot"), ratio = 0.8, parallel = 32L)

  VARSELECT2_temp=print(VARSELECT2_1$variable.importance)
  inds<-which(VARSELECT2_temp!=0)
  VARSELECT2=names(VARSELECT2_temp)[inds]

  VAR_SELECT_FINAL=intersect(VARSELECT1,VARSELECT2)
  
  iteration=as.character(i)
  if(i==10){
  ALL_VARIABLES=list(VAR_SELECT_FINAL)
  } else{
  ALL_VARIABLES=append(ALL_VARIABLES,list(VAR_SELECT_FINAL))}

  x.tr=x.tr[,VAR_SELECT_FINAL]
  x.te=x.te[,VAR_SELECT_FINAL]
  
  #Training ML models 
  
  print("starting NN")
  
  nnFit <- caret::train(x.tr,as.factor(y.tr),
                 method='mlp', 
                 trControl=fitControl,
                 metric="ROC",
                 preProc = c("center", "scale"), 
                 tuneGrid = nnGrid)
 
  print("NN finished")
  
  print(nnFit)
  
  classpred=predict(nnFit,x.te,type="prob")
  temp=roc(y.te_num,as.numeric(classpred[,1]))
  test_nn[i/10] = as.numeric(temp$auc)
  

  pr_nn[i/10]=as.numeric(pr.curve(scores.class0 = classpred[,1],weights.class0=y.te_num)[2])

  print(test_nn[i/10])
  print(pr_nn[i/10])

  classpred=predict(nnFit,x.tr,type="prob")
  temp=roc(y.tr_num,as.numeric(classpred[,1]))
  train_nn[i/10] = as.numeric(temp$auc)
  
  pr_train_nn[i/10]=as.numeric(pr.curve(scores.class0 = classpred[,1],weights.class0=y.tr_num)[2])
  
  print("starting GLM")
 
  
  lmFit <- caret::train(x.tr,as.factor(y.tr),
                 method = "glmnet", 
                 trControl = fitControl,
                 metric = "ROC",
                 preProc = c("center", "scale"),
                 tuneGrid = lmGrid)

  print("GLM finished")
  
  print(lmFit)
  
  classpred=predict(lmFit,x.te,type="prob")
  temp=roc(y.te_num,as.numeric(classpred[,1]))
  test_glm[i/10] = as.numeric(temp$auc)
  
  pr_glm[i/10]=as.numeric(pr.curve(scores.class0 = classpred[,1],weights.class0=y.te_num)[2])
  
  print(test_glm[i/10])
  print(pr_glm[i/10])
  
  classpred=predict(lmFit,x.tr,type="prob")
  temp=roc(y.tr_num,as.numeric(classpred[,1]))
  train_glm[i/10] = as.numeric(temp$auc)
  
  pr_train_glm[i/10]=as.numeric(pr.curve(scores.class0 = classpred[,1],weights.class0=y.tr_num)[2])
  
  print("starting XGBoost")
  
  registerDoSEQ()
  
  xgbFit <- caret::train(x.tr,as.factor(y.tr),
                      method='xgbTree', 
                      trControl=fitControl,
                      metric="ROC",
                      preProc = c("center", "scale"), 
                      tuneGrid = xgbGrid)
  
  print("XGBoost finished")
  
  print(xgbFit)
  
  classpred=predict(xgbFit,x.te,type="prob")
  
  print("XGBoost confusion matrix")
  
  pred = ifelse(classpred < 0.5, "N", "E")
  print(caret::confusionMatrix(as.factor(pred[,1]),as.factor(y.te), mode = "sens_spec"))
  
  temp=roc(y.te_num,as.numeric(classpred[,1]))
  test_xgb[i/10] = as.numeric(temp$auc)
  
  pr_xgb[i/10]=as.numeric(pr.curve(scores.class0 = classpred[,1],weights.class0=y.te_num)[2])
  
  classpred=predict(xgbFit,x.tr,type="prob")
  temp=roc(y.tr_num,as.numeric(classpred[,1]))
  train_xgb[i/10] = as.numeric(temp$auc)
  
  pr_train_xgb[i/10]=as.numeric(pr.curve(scores.class0 = classpred[,1],weights.class0=y.tr_num)[2])
  
  print(test_xgb[i/10])
  print(pr_xgb[i/10])
  
  registerDoMC(cores = 32)
  
  print("starting GBM")
  
  gbmFit <- caret::train(x.tr,as.factor(y.tr), 
                  method = "gbm",
                  distribution="bernoulli", 
                  trControl = fitControl,
                  preProc = c("center", "scale"),
                  metric="ROC",
                  verbose = FALSE,
                  tuneGrid = gbmGrid)
 
  print("GBM finished")
  
  print(gbmFit)
  
  classpred=predict(gbmFit,x.te,type="prob")
  temp=roc(y.te_num,as.numeric(classpred[,1]))
  test_gbm[i/10] = as.numeric(temp$auc)
  
  pr_gbm[i/10]=as.numeric(pr.curve(scores.class0 = classpred[,1],weights.class0=y.te_num)[2])
  
  print(test_gbm[i/10])
  print(pr_gbm[i/10])
  
  classpred=predict(gbmFit,x.tr,type="prob")
  temp=roc(y.tr_num,as.numeric(classpred[,1]))
  train_gbm[i/10] = as.numeric(temp$auc)
  
  pr_train_gbm[i/10]=as.numeric(pr.curve(scores.class0 = classpred[,1],weights.class0=y.tr_num)[2])
  
  print("starting RF")
  
  rfFit <- caret::train(x.tr,as.factor(y.tr), 
                 method = "rf", 
                 trControl = fitControl,
                 preProc = c("center", "scale"),
                 metric="ROC",
                 tuneGrid = rfGrid)
 
  print("RF finished")
  
  print(rfFit)
  
  classpred=predict(rfFit,x.te,type="prob")
  
  print("RF confusion matrix")
  
  pred = ifelse(classpred < 0.5, "N", "E")
  print(caret::confusionMatrix(as.factor(pred[,1]),as.factor(y.te), mode = "sens_spec"))
  
  temp=roc(y.te_num,as.numeric(classpred[,1]))
  test_rf[i/10] = as.numeric(temp$auc)

  print(head(classpred[,1]))
  print(head(classpred[,2]))
 
  pr_rf[i/10]=as.numeric(pr.curve(scores.class0 = classpred[,1],weights.class0=y.te_num)[2])
  
  print(test_rf[i/10])
  print(pr_rf[i/10])
  
  classpred=predict(rfFit,x.tr,type="prob")
  temp=roc(y.tr_num,as.numeric(classpred[,1]))
  train_rf[i/10] = as.numeric(temp$auc)
  
  pr_train_rf[i/10]=as.numeric(pr.curve(scores.class0 = classpred[,1],weights.class0=y.tr_num)[2])
  
  print("starting SVM")
 
  
  svmFit <- caret::train(x.tr,as.factor(y.tr),
                  method = "svmRadial", 
                  trControl = fitControl, 
                  preProc = c("center", "scale"),
                  tuneGrid = svmGrid,
                  metric = "ROC")

  print("SVM finished")
  
  print(svmFit)
  
  classpred=predict(svmFit,x.te,type="prob")
  
  print("SVM confusion matrix")
  pred = ifelse(classpred < 0.5, "N", "E")
  print(caret::confusionMatrix(as.factor(pred[,1]),as.factor(y.te), mode = "sens_spec"))
  
  temp=roc(y.te_num,as.numeric(classpred[,1]))
  test_svm[i/10] = as.numeric(temp$auc)
  

  print(nrow(x.te))
  print(length(y.te_num))

  print(head(classpred[,1]))
  print(head(classpred[,2]))

  
  pr_svm[i/10]=as.numeric(pr.curve(scores.class0 = classpred[,1],weights.class0=y.te_num)[2])
  
  print(test_svm[i/10])
  print(pr_svm[i/10])
  
  classpred=predict(svmFit,x.tr,type="prob")
  temp=roc(y.tr_num,as.numeric(classpred[,1]))
  train_svm[i/10] = as.numeric(temp$auc)
  
  pr_train_svm[i/10]=as.numeric(pr.curve(scores.class0 = classpred[,1],weights.class0=y.tr_num)[2])
  
  #Default classifier
  
  classpred=sample(0:1, size=length(y.te), prob=c(dim(x2)[1]/dim(x)[1],dim(x1)[1]/dim(x)[1]),replace=TRUE)
  temp=roc(y.te_num,as.numeric(classpred))
  test_default[i/10] = as.numeric(temp$auc)
  
  pred = ifelse(classpred < 0.5, 0, 1)
  pred2= +(!pred)
  
  pr_default[i/10]=as.numeric(pr.curve(scores.class0 = pred2,weights.class0=y.te_num)[2])
  
  classpred=sample(0:1, size=length(y.tr), prob=c(dim(x2)[1]/dim(x)[1],dim(x1)[1]/dim(x)[1]),replace=TRUE)
  temp=roc(y.tr_num,as.numeric(classpred))
  train_default[i/10] = as.numeric(temp$auc)
  
  pred = ifelse(classpred < 0.5, 0, 1)
  pred2= +(!pred)
  
  pr_train_default[i/10]=as.numeric(pr.curve(scores.class0 = pred2,weights.class0=y.tr_num)[2])
  
}

#Save plots, variable importance tables

filename=paste(prefix,"_ROCAUC_TEST.svg",sep="")

training_set_size=seq(10, 90, by = 10)

df <- data.frame(training_set_size=rep(training_set_size,7), ROC_AUC=c(test_svm,test_rf,test_gbm,test_xgb,test_glm,test_nn,test_default), Set=c(rep("SVM_ROC_TEST", 9),rep("RF_ROC_TEST", 9),rep("GBM_ROC_TEST", 9),rep("XGB_ROC_TEST", 9),rep("GLM_ROC_TEST", 9), rep("NN_ROC_TEST", 9), rep("DEFAULT_ROC_TEST", 9)))

svg(filename,width=5, height=5)
ggplot(df, aes(x=training_set_size, y=ROC_AUC, color=Set)) + geom_point(size=4) + scale_x_discrete(limits=c(10,20,30,40,50,60,70,80,90),labels=c("10", "20", "30", "40", "50", "60", "70", "80", "90")) + geom_smooth(se = FALSE, method = lm, size=1.5) + theme_gdocs(base_family="Times",base_size=12) + scale_color_gdocs()
dev.off()


filename=paste(prefix,"_ROCAUC_TRAIN.svg",sep="")

training_set_size=seq(10, 90, by = 10)

df <- data.frame(training_set_size=rep(training_set_size,7), ROC_AUC=c(train_svm,train_rf,train_gbm,train_xgb,train_glm,train_nn,train_default), Set=c(rep("SVM_ROC_TRAIN", 9),rep("RF_ROC_TRAIN", 9),rep("GBM_ROC_TRAIN", 9),rep("XGB_ROC_TRAIN", 9),rep("GLM_ROC_TRAIN", 9), rep("NN_ROC_TRAIN", 9), rep("DEFAULT_ROC_TRAIN", 9)))

svg(filename,width=5, height=5)
ggplot(df, aes(x=training_set_size, y=ROC_AUC, color=Set)) + geom_point(size=4) + scale_x_discrete(limits=c(10,20,30,40,50,60,70,80,90),labels=c("10", "20", "30", "40", "50", "60", "70", "80", "90")) + geom_smooth(se = FALSE, method = lm, size=1.5) + theme_gdocs(base_family="Times",base_size=12) + scale_color_gdocs()
dev.off()

filename=paste(prefix,"_PRAUC_TEST.svg",sep="")

training_set_size=seq(10, 90, by = 10)

df <- data.frame(training_set_size=rep(training_set_size,7), PR_AUC=c(pr_svm,pr_rf,pr_gbm,pr_xgb,pr_glm,pr_nn,pr_default), Set=c(rep("SVM_PR_TEST", 9),rep("RF_PR_TEST", 9),rep("GBM_PR_TEST", 9),rep("XGB_PR_TEST", 9),rep("GLM_PR_TEST", 9), rep("NN_PR_TEST", 9), rep("DEFAULT_PR_TEST", 9)))

svg(filename,width=5, height=5)
ggplot(df, aes(x=training_set_size, y=PR_AUC, color=Set)) + geom_point(size=4) + scale_x_discrete(limits=c(10,20,30,40,50,60,70,80,90),labels=c("10", "20", "30", "40", "50", "60", "70", "80", "90")) + geom_smooth(se = FALSE, method = lm, size=1.5) + theme_gdocs(base_family="Times",base_size=12) + scale_color_gdocs()
dev.off()

filename=paste(prefix,"_PRAUC_TRAIN.svg",sep="")

training_set_size=seq(10, 90, by = 10)

df <- data.frame(training_set_size=rep(training_set_size,7), PR_AUC=c(pr_train_svm,pr_train_rf,pr_train_gbm,pr_train_xgb,pr_train_glm,pr_train_nn,pr_train_default), Set=c(rep("SVM_PR_TRAIN", 9),rep("RF_PR_TRAIN", 9),rep("GBM_PR_TRAIN", 9),rep("XGB_PR_TRAIN", 9),rep("GLM_PR_TRAIN", 9), rep("NN_PR_TRAIN", 9), rep("DEFAULT_PR_TRAIN", 9)))

svg(filename,width=5, height=5)
ggplot(df, aes(x=training_set_size, y=PR_AUC, color=Set)) + geom_point(size=4) + scale_x_discrete(limits=c(10,20,30,40,50,60,70,80,90),labels=c("10", "20", "30", "40", "50", "60", "70", "80", "90")) + geom_smooth(se = FALSE, method = lm, size=1.5) + theme_gdocs(base_family="Times",base_size=12) + scale_color_gdocs()
dev.off()

###########Final feature selection - full set

library(glmnet)

fit.lasso <- glmnet(x, labels_, family="binomial", alpha=1)
fit.ridge <- glmnet(x, labels_, family="binomial", alpha=0)
fit.elnet <- glmnet(x, labels_, family="binomial", alpha=.5)

for (i in seq(0,10,by=5)) {
  assign(paste("fit", i, sep=""), cv.glmnet(x, labels_, type.measure="auc", 
                                            alpha=i/10,family="binomial",parallel = TRUE))
}

filename=paste(prefix,"_FS_plot.svg",sep="")
svg(filename,width=5, height=5)


par(mfrow=c(3,2))

plot(fit.lasso, xvar="lambda")
plot(fit10, main="LASSO")

plot(fit.ridge, xvar="lambda")
plot(fit0, main="Ridge")

plot(fit.elnet, xvar="lambda")
plot(fit5, main="Elastic Net")

dev.off()


elastic1=as.matrix(coef(fit5,fit5$lambda.min))

inds<-which(elastic1!=0)
VARSELECT1_temp=row.names(elastic1)[inds]
VARSELECT1=VARSELECT1_temp[!(VARSELECT1_temp %in% '(Intercept)')];

library("enpls")

VARSELECT2_1=enspls.fs(x, labels_, maxcomp = 10L, cvfolds = 5L, alpha = seq(0.2, 0.8, 0.2),
                       reptimes = 10L, method = c("mc", "boot"), ratio = 0.8, parallel = 32L)

VARSELECT2_temp=print(VARSELECT2_1)
inds<-which(VARSELECT2_temp!=0)
VARSELECT2=row.names(VARSELECT2_temp)[inds]

VAR_SELECT_FINAL=intersect(VARSELECT1,VARSELECT2)

x1=x1[,VAR_SELECT_FINAL]
x2=x2[,VAR_SELECT_FINAL]
x=x[,VAR_SELECT_FINAL]


###########Training final models


nnFit <- caret::train(x,as.factor(labels),
                      method='mlp', 
                      trControl=fitControl,
                      metric="ROC",
                      preProc = c("center", "scale"), 
                      tuneGrid = nnGrid)


lmFit <- caret::train(x,as.factor(labels),
                      method = "glmnet", 
                      trControl = fitControl,
                      metric = "ROC",
                      preProc = c("center", "scale"),
                      tuneGrid = lmGrid)


gbmFit <- caret::train(x,as.factor(labels), 
                       method = "gbm",
                       distribution="bernoulli", 
                       trControl = fitControl,
                       preProc = c("center", "scale"),
                       metric="ROC",
                       tuneGrid = gbmGrid)


rfFit <- caret::train(x,as.factor(labels), 
                      method = "rf", 
                      trControl = fitControl,
                      preProc = c("center", "scale"),
                      metric="ROC",
                      tuneGrid = rfGrid)

svmFit <- caret::train(x,as.factor(labels),
                       method = "svmRadial", 
                       trControl = fitControl, 
                       preProc = c("center", "scale"),
                       tuneGrid = svmGrid,
                       metric = "ROC")

registerDoSEQ()

xgbFit <- caret::train(x,as.factor(labels),
                       method = "xgbTree", 
                       trControl = fitControl, 
                       preProc = c("center", "scale"),
                       tuneGrid = xgbGrid,
                       metric = "ROC")


library("gbm")
library("randomForest")
library("glmnet")
library("klaR")
library("RSNNS")
library("kernlab")


filename=paste(prefix,"_variable_importance.csv",sep="")

variable_importance=rbind(c("GBM","XGB","RF","SVM","GLM","NN"),cbind(varImp(gbmFit,scale=TRUE)$importance,varImp(xgbFit,scale=TRUE)$importance,varImp(rfFit,scale=TRUE)$importance,varImp(svmFit,scale=TRUE)$importance[1],varImp(lmFit,scale=TRUE)$importance,varImp(nnFit,scale=TRUE)$importance[1]))
colnames(variable_importance)=c()
write.csv(variable_importance,filename)

filename=paste(prefix,"_GBMFIT.svg",sep="")
svg(filename,width=5, height=5)
ggplot(gbmFit)
dev.off()

filename=paste(prefix,"_RFFIT.svg",sep="")
svg(filename,width=5, height=5)
ggplot(rfFit)
dev.off()

filename=paste(prefix,"_SVMFIT.svg",sep="")
svg(filename,width=5, height=5)
ggplot(svmFit)
dev.off()

filename=paste(prefix,"_GLMFIT.svg",sep="")
svg(filename,width=5, height=5)
ggplot(lmFit)
dev.off()

filename=paste(prefix,"_NNFIT.svg",sep="")
svg(filename,width=5, height=5)
ggplot(nnFit)
dev.off()

filename=paste(prefix,"_XGBFIT.svg",sep="")
svg(filename,width=5, height=5)
ggplot(xgbFit)
dev.off()

